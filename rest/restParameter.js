const axios = require('axios')
const config = require('../config.js')

const  ip = config.url;
axios.defaults.headers.common['Origin'] = config.header;

// const  ip = 'http://tang:tang@localhost:31228/app/rest/';
// axios.defaults.headers.common['Origin'] = 'http://localhost:31228';





// var VCSxml ='<vcs-root id="Sky_Test" name="test" vcsName="jetbrains.git" href="/app/rest/vcs-roots/id:Sky_Sun">\
// <project id="Sky" name="sky" parentProjectId="_Root" href="/app/rest/projects/id:Sky" webUrl="http://localhost:31228/project.html?projectId=Sky"/>\
// <properties count="9">\
//   <property name="agentCleanFilesPolicy" value="ALL_UNTRACKED"/>\
//   <property name="agentCleanPolicy" value="ON_BRANCH_CHANGE"/>\
//   <property name="authMethod" value="ANONYMOUS"/>\
//   <property name="branch" value="refs/heads/master"/>\
//   <property name="ignoreKnownHosts" value="true"/>\
//   <property name="submoduleCheckout" value="CHECKOUT"/>\
//   <property name="url" value="https://gitlab.com/SoHigh/sun.git"/>\
//   <property name="useAlternates" value="true"/>\
//   <property name="usernameStyle" value="USERID"/>\
// </properties>\
// </vcs-root>';



// var attachxml ='<vcs-root-entry id="Sky_Test" inherited="true">\
// <vcs-root id="Sky_Test" />\
// <checkout-rules></checkout-rules>\
// </vcs-root-entry>';

// var triggerxml='<build>\
// <buildType id="Sky_Test"/>\
// </build>';



module.exports = {
    
  
  newBuild: function (name) {
    axios.post(ip + 'projects/id:Sky/buildTypes',{ "name":name}   , 
 {  headers:
  {
    //'Origin':'http://localhost:31228',
 'Content-Type': 'application/json'
 } 
 } )
     .then((res) => {
    // console.log(res);
     console.log('new build success!');
 })
 .catch((err) => {
     console.log(err);
 });

    },
  
  ///////////
  
  newVCS: function (name,url) {
    var VCSxml=      '<vcs-root id="Sky_'+ name + '" name="'+name+ '" vcsName="jetbrains.git" href="/app/rest/vcs-roots/id:Sky_' +name+'">'+
'<project id="Sky" name="sky" parentProjectId="_Root" href="/app/rest/projects/id:Sky" webUrl="http://localhost:31228/project.html?projectId=Sky"/>'+
'<properties count="9">\
  <property name="agentCleanFilesPolicy" value="ALL_UNTRACKED"/>\
  <property name="agentCleanPolicy" value="ON_BRANCH_CHANGE"/>\
  <property name="authMethod" value="ANONYMOUS"/>\
  <property name="branch" value="refs/heads/master"/>\
  <property name="ignoreKnownHosts" value="true"/>\
  <property name="submoduleCheckout" value="CHECKOUT"/>'+
  '<property name="url" value="' +url +'"/>'+
  '<property name="useAlternates" value="true"/>\
  <property name="usernameStyle" value="USERID"/>\
</properties>\
</vcs-root>';  
    
    axios.post(ip + 'vcs-roots',
    VCSxml
  
    
    , 
 {
  headers:
  {
    //'Origin':'http://localhost:31228',
 'Content-Type': 'application/xml'
 } 
 })
     .then((res) => {
    // console.log(res);
     console.log('add vcs-root success!');
 })
 .catch((err) => {
     console.log(err);
 });

    },


/////////
    attachVCS: function (name) {
      var attachxml ='<vcs-root-entry id="Sky_'+ name +'" inherited="true">\
      <vcs-root id="Sky_'+ name + '" />\
      <checkout-rules></checkout-rules>\
      </vcs-root-entry>';
      
      
   return   axios.post(ip + 'buildTypes/id:Sky_'+name+ '/vcs-root-entries/',attachxml, 
   {
    headers:
    {
    
   'Content-Type': 'application/xml'
   }    })
       .then((res) => {
      // console.log(res);
       console.log('attach vcs-root success!');
        return "DONE"
   })
   .catch((err) => {
      // console.log(err);
      return "Fall"
   });
  





      },

///////////////
  
setParameter: function (name) {
return  axios.put(ip + 'buildTypes/id:Sky_'+name+'/parameters/NAME/',{"value":name}, 
{  headers:
{
 
'Content-Type': 'application/json'
} 
} )
   .then((res) => {
  // console.log(res);
   console.log('set parameter success!');
   return "DONE"
})
.catch((err) => {
  // console.log(err);
   return "Fall"
});

  },


  ////////
  trigger: function (name) {
        

    var triggerxml='<build>\
<buildType id="Sky_'+name+'"/>\
</build>';


  return  axios.post(ip + 'buildQueue', triggerxml, 
 {
  headers:
  {
  
 'Content-Type': 'application/xml'
 } 
 })
     .then((res) => {
    // console.log(res);
     console.log('trigger success!');
     return "DONE" 
 })
 .catch((err) => {
     //console.log(err);
     return "Fall"
 });

    },

//////////////


deleteVCS: function (name) {
 



  return axios.delete(ip + 'vcs-roots/id:Sky_'+name   )
   .then((res) => {
  // console.log(res);
   console.log('delete VCS success!');
    return "DONE"
   
  })
.catch((err) => {

   //console.log(err);
    return "Fall"
  });



},



  //////////////
  deleteBuild: function (name) {
    axios.delete(ip + 'buildTypes/id:Sky_'+name   )
     .then((res) => {
     //console.log(res);
     console.log('delete build success!');
     
  })
  .catch((err) => {
     //console.log(err);
  });
    return 0;
    },









  //////////////
  setDeleteParameter: function (name) {
    return  axios.put(ip + 'buildTypes/id:Sky_AutoDelete/parameters/NAME/',{"value":name}, 
    {  headers:
    {
     
    'Content-Type': 'application/json'
    } 
    } )
       .then((res) => {
      // console.log(res);
       console.log('set auto delete parameter success!');
       return "DONE"
    })
    .catch((err) => {
      // console.log(err);
       return "Fall"
    });
    
      },
//////////////////////////    
    bar: function () {
      // whatever
    }





  };
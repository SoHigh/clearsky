# This file is a template, and might need editing before it works on your project.
FROM node:8.12

ARG configfile=cloud-config.js

WORKDIR /usr/src/app

ENV PORT 5000

COPY package.json /usr/src/app

RUN npm install

COPY . /usr/src/app 

RUN rm /usr/src/app/config.js

COPY $configfile /usr/src/app/config.js

# replace this with your application's default port

EXPOSE 5000

CMD [ "npm", "start" ]
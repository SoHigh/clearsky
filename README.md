
### Problem that have encourtered 
2018/12
- Can not access eScience Openstack's instance with secure shell.
    - Fixed. Wrong username.
- Can not install Kubernetes on instace no matter why to install.
    - Swith to Google cloud.
- Kubernetes Dashboard can not be access due to role base account.
    - Create service account for the Dashboard. 
- Helm can not order cluster due to role base account.
    - Create service account for Helm.
- Not enough credit to do this project on Google cloud.
    - Swith to local nodebook 

2019/1
- Teamcity agent some how does not have docker.
    - Change referance yaml file to create Teamcity server and agent on cluster.
- Teamcity server lost all configuration after restart notebook.
    - Create persistent volume on cluster for Teamcity server.
- Build pack fail to auto build image on "core" program due to agent change its directory while building.
    - Create persistent volume on cluster for Teamcity agent.
- Every thing run so slow due to poor spec of poor student's notebook.
    - Grin and bear, poor me.
- Teamcity agent does not have helm.
    - Add command to install helm at the start of the container
- Teamcity agent's helm can not connect to Kubernetes cluster even after used "helm init --client-only" command.
    - Use "helm init --upgrade" instead.

2019/2
- Docker image registry with just http without s can not be use unless change config.
    - Try not using image registry.
- Auto build command script (referance) somewhat complicated. Hope I can understand soon.
    - Switch to simple one and start from scratch.



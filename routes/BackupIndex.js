var express = require('express');
var router = express.Router();
//const axios = require('axios')
//const  ip = 'http://tang:tang@localhost:31228/app/rest/';
var rest = require('../rest/rest');
var restP = require('../rest/restParameter');
const config = require('../config.js')
//const K8s = require('easy-k8s').Client;
//const kubeconfig = require('../kubeconfig');

//const K8sConfig = require('easy-k8s').config;

//const client = new Client({ config: config.fromKubeconfig(kubeconfig), version: '1.13' });

//in cluster
var client;
try {
  const config = require('kubernetes-client').config;
const Client = require('kubernetes-client').Client;

 client = new Client({ config: config.getInCluster() })

} catch (error) {
  console.log(error);
}

// on localhost
var client2
try {
  const Client2 = require('kubernetes-client').Client
const config2 = require('kubernetes-client').config
client2 = new Client2({ config: config2.fromKubeconfig(), version: '1.13' })

} catch (error) {
  console.log(error);
}




/* POST to add */
router.post('/addrepository',async  function(req, res) {
  //Get our form values. download url variable
  var count=0;
  res.redirect(config.redirectPath);
  var name = req.body.name;
  var url = req.body.url;
  var casedName= "";
  if (casedName.length ==1){
    casedName= name.charAt(0).toUpperCase() ;
  }
  else if  (casedName.length >1){
    casedName= name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
  }
  await restP.newBuild(casedName);
  await restP.newVCS(casedName,url);
  // await restP.attachVCS(casedName);
  // await restP.setParameter(casedName);
  // await restP.trigger(casedName);
  while (count<10) {
    let Result = await restP.attachVCS(casedName);
    console.log(Result);
    if (Result == "DONE") {
      break;
    }
    count+=1;
  }
  while (count<10) {
    let Result = await restP.setParameter(casedName);
    console.log(Result);
    if (Result == "DONE") {
      break;
    }
    count+=1;
  }
  while (count<10) {
    let Result = await restP.trigger(casedName);
    console.log(Result);
    if (Result == "DONE") {
      break;
    }
    count+=1;
  }





  

});
 
/* POST to delete */
router.post('/deleterepository',async  function(req, res) {
  //Get our form values. download url variable
 var count=0;
  var name = req.body.deployed;
  var casedName= name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
  res.redirect(config.redirectPath);
  
  await restP.deleteBuild(casedName);
  //await restP.deleteVCS(casedName);  

  while (count<10) {
    let Result = await restP.deleteVCS(casedName);
    console.log(Result);
    if (Result == "DONE") {
      break;
    }
    count+=1;
  }
  await restP.trigger("AutoDelete");
   ///setTimeout(restP.deleteVCS(casedName), 100000);

 // res.redirect('/deployment-local');

  

});
router.post('/confirmation',async  function(req, res) {
  //Get our form values. download url variable
  //res.redirect("/comfirm");
  var name = req.body.name;
  var casedName= name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();

  await restP.setDeleteParameter(casedName.toLowerCase());

  res.render('confirm', { 'title': 'Delete Confirmation', 'deployed' : casedName });
  
  //console.log(req.body.name);

});

router.post('/test',async  function(req, res) {
  //Get our form values. download url variable
  //res.redirect("/comfirm");
  //var name = req.body.name;
  //var casedName= name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
  //res.render('confirm', { 'title': 'Delete Confirmation', 'deployed' : casedName });
  
  console.log(req.body.deployed);

}); 
 
 //test teamcity api move to rest.js
//  axios.defaults.headers.common['Origin'] = 'http://localhost:31228';
//  function newVCS() {  

//   var xml ='<vcs-root id="Sky_Test" name="test" vcsName="jetbrains.git" href="/app/rest/vcs-roots/id:Sky_Sun">\
//   <project id="Sky" name="sky" parentProjectId="_Root" href="/app/rest/projects/id:Sky" webUrl="http://localhost:31228/project.html?projectId=Sky"/>\
//   <properties count="9">\
//     <property name="agentCleanFilesPolicy" value="ALL_UNTRACKED"/>\
//     <property name="agentCleanPolicy" value="ON_BRANCH_CHANGE"/>\
//     <property name="authMethod" value="ANONYMOUS"/>\
//     <property name="branch" value="refs/heads/master"/>\
//     <property name="ignoreKnownHosts" value="true"/>\
//     <property name="submoduleCheckout" value="CHECKOUT"/>\
//     <property name="url" value="https://gitlab.com/SoHigh/sun.git"/>\
//     <property name="useAlternates" value="true"/>\
//     <property name="usernameStyle" value="USERID"/>\
//   </properties>\
//  </vcs-root>'
 
 
//  axios.post(ip + 'vcs-roots',xml, 
//  {
//   headers:
//   {
//     //'Origin':'http://localhost:31228',
//  'Content-Type': 'application/xml'
//  } 
//  })
//      .then((res) => {
//      console.log(res);
//      console.log('add vcs-root success!');
//  })
//  .catch((err) => {
//      console.log(err);
//  });


// }





 
// router.get('/addrepository',async function(req, res) {
//   //res.render('index', { title: 'Clear Sky' });
//   var url = req.body.url;


//   await rest.newBuild();
//   await rest.newVCS();
//   await rest.attachVCS();
//   await rest.setParameter();
//   await rest.trigger();


//   res.redirect('/deployment-local');





// });




/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Clear Sky' });
  //console.log(kubeconfig);
  //console.log('///');
  //console.log(config);

});







///k8s part

// router.get('/allpod',async function(req, res) {
//     //Get our form values. download url variable
//   //var  allPodSpecs = await K8s.get(kubeconfig, 'all', 'pod');
//   //var  namespaces = await client.api.v1.namespaces.get()
//   var deployment = await client.apis.apps.v1.deployments.get()
//   //console.log(allPodSpecs);
//   console.log( deployment);
//   //console.log(allPodSpecs.body.items);
//   });



  router.get('/deployment',async function(req, res) {
    //Get our form values. download url variable
  //var  alldeployment = await K8s.get(kubeconfig, 'default', 'deployment');
  await client.loadSpec();
  var alldeployment = await client.apis.apps.v1.namespaces("sky").deployments.get();
  //console.log(alldeployment.body.items );
  //await restP.setDeleteParameter("");
  res.render('main', { 'title': 'Clear Sky', 'values' : alldeployment.body.items });

  });



  router.get('/deployment-local',async function(req, res) {
    //Get our form values. download url variable
  //var  alldeployment = await K8s.get(kubeconfig, 'default', 'deployment');
  //await client.loadSpec();
  var alldeployment = await client2.apis.apps.v1.namespaces("sky").deployments.get();
  var allservice =  await client2.api.v1.namespaces("sky").services.get();
  //console.log(alldeployment.body.items[0].status );
  //console.log( "///////////cut/////////////////////");
  //console.log( allservice.body.items[0].spec.ports[0].nodePort );

  //await restP.setDeleteParameter("");
  res.render('main', { 'title': 'Clear Sky', 'deployments' : alldeployment.body.items ,'ports':allservice.body.items});

  });




module.exports = router;

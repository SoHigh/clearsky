var express = require('express');
var router = express.Router();
//const axios = require('axios')
//const  ip = 'http://tang:tang@localhost:31228/app/rest/';

var restP = require('../rest/restParameter');
const config = require('../config.js')

//in cluster
var client;
try {
  const config = require('kubernetes-client').config;
const Client = require('kubernetes-client').Client;

 client = new Client({ config: config.getInCluster() })

} catch (error) {
  console.log(error);
}

// on localhost
var client2
try {
  const Client2 = require('kubernetes-client').Client
const config2 = require('kubernetes-client').config
client2 = new Client2({ config: config2.fromKubeconfig(), version: '1.13' })

} catch (error) {
  console.log(error);
}




/* POST to add */
router.post('/addrepository',async  function(req, res) {
  //Get our form values. download url variable
  var count=0;
  res.redirect(config.redirectPath);
  var name = req.body.name;
  var url = req.body.url;
  var casedName= "";
  if (name.length ==1){
    casedName= name.charAt(0).toUpperCase() ;
  }
  else if  (name.length >1){
    casedName= name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
  }
  await restP.newBuild(casedName);
  await restP.newVCS(casedName,url);
  // await restP.attachVCS(casedName);
  // await restP.setParameter(casedName);
  // await restP.trigger(casedName);
  while (count<10) {
    let Result = await restP.attachVCS(casedName);
    console.log(Result);
    if (Result == "DONE") {
      break;
    }
    count+=1;
  }
  while (count<10) {
    let Result = await restP.setParameter(casedName);
    console.log(Result);
    if (Result == "DONE") {
      break;
    }
    count+=1;
  }
  while (count<10) {
    let Result = await restP.trigger(casedName);
    console.log(Result);
    if (Result == "DONE") {
      break;
    }
    count+=1;
  }





  

});
 
/* POST to delete */
router.post('/deleterepository',async  function(req, res) {
  //Get our form values. download url variable
 var count=0;
  var name = req.body.deployed;
  var casedName= "";
  if (name.length ==1){
    casedName= name.charAt(0).toUpperCase() ;
  }
  else if  (name.length >1){
    casedName= name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
  }
 
  
  await restP.deleteBuild(casedName);
  //await restP.deleteVCS(casedName);  

  while (count<10) {
    let Result = await restP.deleteVCS(casedName);
    console.log(Result);
    if (Result == "DONE") {
      break;
    }
    count+=1;
  }
  await restP.trigger("AutoDelete");
   ///setTimeout(restP.deleteVCS(casedName), 100000);
   res.redirect(config.redirectPath);
 // res.redirect('/deployment-local');

  

});
router.post('/confirmation',async  function(req, res) {
  //Get our form values. download url variable
  //res.redirect("/comfirm");
  var name = req.body.name;
  var casedName= "";
  if (name.length ==1){
    casedName= name.charAt(0).toUpperCase() ;
  }
  else if  (name.length >1){
    casedName= name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
  }
  await restP.setDeleteParameter(casedName.toLowerCase());

  res.render('confirm', { 'title': 'Delete Confirmation', 'deployed' : casedName });
  
  //console.log(req.body.name);

});

router.post('/test',async  function(req, res) {
  //Get our form values. download url variable
  //res.redirect("/comfirm");
  //var name = req.body.name;
  //var casedName= name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
  //res.render('confirm', { 'title': 'Delete Confirmation', 'deployed' : casedName });
  
  console.log(req.body.deployed);

}); 
 




/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Clear Sky' });
//   //console.log(kubeconfig);
//   //console.log('///');
//   //console.log(config);

// });







///k8s part



  router.get(config.clusterPath,async function(req, res) {
    //Get our form values. download url variable
  //var  alldeployment = await K8s.get(kubeconfig, 'default', 'deployment');
  await client.loadSpec();
  var alldeployment = await client.apis.apps.v1.namespaces("sky").deployments.get();
  var allservice =  await client.api.v1.namespaces("sky").services.get();
  //console.log(alldeployment.body.items );
  //await restP.setDeleteParameter("");
  res.render('main', { 'title': 'Clear Sky', 'deployments' : alldeployment.body.items ,'ports':allservice.body.items});

  });



  router.get(config.localPath,async function(req, res) {
    //Get our form values. download url variable
  //var  alldeployment = await K8s.get(kubeconfig, 'default', 'deployment');
  //await client.loadSpec();
  var alldeployment = await client2.apis.apps.v1.namespaces("sky").deployments.get();
  var allservice =  await client2.api.v1.namespaces("sky").services.get();
  //console.log(alldeployment.body.items[0].status );
  //console.log( "///////////cut/////////////////////");
  //console.log( allservice.body.items[0].spec.ports[0].nodePort );

  //await restP.setDeleteParameter("");
  res.render('main', { 'title': 'Clear Sky', 'deployments' : alldeployment.body.items ,'ports':allservice.body.items});

  });




module.exports = router;
